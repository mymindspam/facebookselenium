# -*- coding: utf-8 -*-
from scrapy import Spider
from selenium import webdriver
from os import path
from scrapy.http import Request

class SeleniumSpider(Spider):
    name = "selenium"

    driver = None

    def __init__(self, driver, *args, **kwargs):
        super(SeleniumSpider, self).__init__(*args, **kwargs)
        pwd = path.dirname(path.realpath(__file__))
        if driver == "phantomjs":
            driver = webdriver.PhantomJS(
                executable_path=path.join(pwd, "../../bin/phantomjs")
            )
        elif driver == "chrome":
            driver = webdriver.Chrome(executable_path=path.join(pwd, "../../bin/chromedriver"))
        self.driver = driver

    def __del__(self):
        self.driver.close()