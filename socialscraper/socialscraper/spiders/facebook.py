# -*- coding: utf-8 -*-
from scrapy import Spider, FormRequest, Request
from socialscraper.spiders.base import SeleniumSpider
from socialscraper.items import PersonItem
import re

class FacebookSpider(SeleniumSpider):
    name = "facebook"
    allowed_domains = ["facebook.com"]

    driver = None
    FIND_FRIENDS_URL = "https://www.facebook.com/find-friends/browser/"
    EMAIL_REGEXP = re.compile(r'[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}')
    PHONE_REGEXP = re.compile(r'(\d{3}[-\.\s]??\d{3}[-\.\s]??\d{4}|\(\d{3}\)\s*\d{3}[-\.\s]??\d{4}|\d{3}[-\.\s]??\d{4})')
    ID_REGEXP = re.compile(r'facebook.com/([A-Za-z.0-9]+)')
    ID_NAME_REGEXP = re.compile(r'facebook.com/profile.php\?id=(\d+)')

    def __init__(self, city, user, password, *args, **kwargs):
        super(FacebookSpider, self).__init__(*args, **kwargs)
        self._login(user, password)
        self.start_urls = [self.FIND_FRIENDS_URL]

    def _login(self, user, password):
        self.driver.get("http://www.facebook.org")

        self.driver.find_element_by_id("email").send_keys(user)
        self.driver.find_element_by_id("pass").send_keys(password)
        self.driver.find_element_by_id("login_form").submit()

    def parse(self, response):
        wrong_links = ["https://www.facebook.com/notifications", "https://www.facebook.com/find-friends/browser/#", ""]
        self.driver.get(self.FIND_FRIENDS_URL)
        person_item_xpath = "//div[contains(class, friendBrowserCheckboxContentGrid)]/ul/li/div/div/div[contains(class, friendBrowserContentAlignMiddle)]/div/a"
        links = self.driver.find_elements_by_xpath(person_item_xpath)
        links = [item.get_attribute("href") for item in links if item.get_attribute("href") not in wrong_links]
        for link in links:
            link = self._prepare_profile_link(link)
            self.driver.get(link)
            self.log('Scraped from <200 %s>' % link)
            name = self.driver.find_element_by_xpath("//*[@id='fb-timeline-cover-name']").text
            body = self.driver.page_source
            phone = self._exctract_phone_numbers(body)
            email = self._exctract_emails(body)
            yield PersonItem(
                name=name,
                phone=phone,
                email=email,
            )

    def _prepare_profile_link(self, link):
        id = self.ID_NAME_REGEXP.search(link)
        if id:
            id = id.group(1)
            return "https://www.facebook.com/profile.php?id=%s&sk=about" % id
        else:
            id = self.ID_REGEXP.search(link).group(1)
            return "https://www.facebook.com/%s/about?section=contact-info&pnref=about" % id

    def _exctract_phone_numbers(self, body):
        phone = [item.text for item in self.driver.find_elements_by_xpath("//span[@dir='ltr']")]
        if len(phone) > 0:
            return phone
        phone = self.PHONE_REGEXP.search(body)
        if phone:
            return phone.groups()
        else:
            return None


    def _exctract_emails(self, body):
        email = self.EMAIL_REGEXP.search(body)
        if email:
            return email.groups()
