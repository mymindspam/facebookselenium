# Deprecated
def selenium(f, selenium):
    def wrapper(self, response):
        response = selenium.get(response.url)
        return f(self, response)
    return wrapper