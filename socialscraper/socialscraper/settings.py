# -*- coding: utf-8 -*-

# Scrapy settings for socialscraper project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'socialscraper'

SPIDER_MODULES = ['socialscraper.spiders']
NEWSPIDER_MODULE = 'socialscraper.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'socialscraper (+http://www.yourdomain.com)'
